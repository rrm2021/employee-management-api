import mongoose from "mongoose";
const { Schema } = mongoose;
// Creating Product schema

const schema = new Schema({
  
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
    required: true,
  },
  title: {
    type: String,
    required: true,
  },
  subTitle: {
    type: String,
    required: true,
  },
  Description: {
    type: String,
    required: true,
  },
  date: {
    type: String,
    required: true,
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  isDeleted: {
    type: Boolean,
    default: false,
  },
  createdDate: {
    type: Date,
    default: Date.now,
  },
});

const Post = mongoose.model("post", schema);
export default Post;
