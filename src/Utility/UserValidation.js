// Add Validation
module.exports.addUser = function (req, res) {
  // req.check('title', 'Please Enter Title').notEmpty();
  // req.check('author', 'Please Enter Author').notEmpty();
  // req.check('description', 'Please Enter Description').notEmpty();
  // Store errors
  // var errors=req.validationErrors();
  var errors = true;
  console.log("errors", errors);
  // cb(errors);
  if (errors) {
    return res.status(400).json({ msg: "Parameter missing!!!", ack: 0 });
  } else {
    return true;
  }
};

// Edit Validation
module.exports.edit = function (req, res, cb) {
  req.check("title", "Please Enter Title").notEmpty();
  req.check("author", "Please Enter Author").notEmpty();
  req.check("description", "Please Enter Description").notEmpty();
  // Store errors
  var errors = req.validationErrors();
  cb(errors);
};
