import express from 'express';
import UserController from '../Controllers/UserController';
// intializing express router
const router = express.Router();

router.post('/user', UserController.createUser);
router.get('/users', UserController.usersLists);
router.get('/user', UserController.userDetails);
router.put('/user', UserController.userUpdate);
router.delete('/user', UserController.userDelete);
export default router;