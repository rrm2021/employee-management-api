import express from 'express';
import user from './UserRoute';
import post from './PostRoute';

// intializing express router
const router = express.Router();
// intializing express with JSON
router.use(express.json())

router.use('/user', user);
router.use('/post', post);

export default router;
