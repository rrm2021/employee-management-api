import express from 'express';
import PostController from '../Controllers/PostController';
// intializing express router
const router = express.Router();

router.post('/post', PostController.createPost);
router.get('/posts', PostController.listsPosts);
router.get('/post', PostController.postDetails);
router.put('/post', PostController.postUpdate);
router.delete('/post', PostController.postDelete);
export default router;