import Post from "../../Models/Post";
import moment from "moment";
var mongoose = require("mongoose");

/**
 * createPost
 * @param req body
 * @returns JSON
 */
const createPost = async (req, res) => {
  if (
    req.body.userId == null ||
    req.body.title == null ||
    req.body.subTitle == null ||
    req.body.Description == null ||
    req.body.date == null
  ) {
    return res.status(400).json({ msg: "Parameter missing!!!" });
  }
  try {
    const allData = req.body;
    const addCar = await new Post(allData).save();
    res.status(200).json({ msg: "Post added successfully" });
  } catch (err) {
    console.log("Error => ", err.message);
    res.status(500).json({ msg: "Something went wrong." });
  }
};

/**
 * listsPosts
 * @param req body
 * @returns json
 */
const listsPosts = async (req, res) => {
  if (req.query.limit == null || req.query.page == null) {
    return res.status(400).json({ msg: "Parameter missing!!!", ack: 0 });
  }
  try {
    let filterData = {
      isActive: true,
      isDeleted: false,
    };
    let limit = parseInt(req.query.limit);
    let page = req.query.page;
    let skip = limit * page;
    const posts = await Post.aggregate([
      {
        $match: filterData,
      },
      {
        $lookup: {
          from: "users",
          localField: "userId",
          foreignField: "_id",
          as: "userDetails",
        },
      },
      { $sort: { createdDate: -1 } },
      { $skip: skip },
      { $limit: limit },
    ]);
    res
      .status(200)
      .json({ data: posts, msg: "Post List found successfully", ack: 1 });
  } catch (err) {
    console.log("Error => ", err.message);
    res.status(500).json({ msg: "Something went wrong.." });
  }
};

/**
 * postDetails
 * @param req body
 * @returns json
 */
const postDetails = async (req, res) => {
  if (req.query.postId == null) {
    return res.status(400).json({ msg: "Parameter missing!!!", ack: 0 });
  }
  try {
    let filterData = {
      isActive: true,
      isDeleted: false,
      _id: mongoose.Types.ObjectId(req.query.postId),
    };
    const posts = await Post.aggregate([
      {
        $match: filterData,
      },
      {
        $lookup: {
          from: "users",
          localField: "userId",
          foreignField: "_id",
          as: "userDetails",
        },
      },
    ]);

    if (posts) {
      res
        .status(200)
        .json({ data: posts, msg: "Post Details found successfully", ack: 1 });
    } else {
      res.status(200).json({ data: posts, msg: "No Post Found", ack: 0 });
    }
  } catch (err) {
    console.log("Error => ", err.message);
    res.status(500).json({ msg: "Something went wrong.." });
  }
};

/**
 * postUpdate
 * @param req body
 * @returns json
 */
const postUpdate = async (req, res) => {
  if (req.query.postId == null) {
    return res.status(400).json({ msg: "Parameter missing!!!", ack: 0 });
  }
  try {
    const allData = req.body;
    let filterData = {
      isActive: true,
      isDeleted: false,
      _id: mongoose.Types.ObjectId(req.query.postId),
    };
    console.log("allData", allData)
    const posts = await Post.findOneAndUpdate(
      filterData,
      { $set: allData },
      {
        returnOriginal: false,
      }
    );
    if (posts) {
      res.status(200).json({
        data: posts,
        msg: "Post Details Updated successfully",
        ack: 1,
      });
    } else {
      res.status(200).json({ data: posts, msg: "No Post Found", ack: 0 });
    }
  } catch (err) {
    console.log("Error => ", err.message);
    res.status(500).json({ msg: "Something went wrong.." });
  }
};

/**
 * postDelete
 * @param req body
 * @returns json
 */
const postDelete = async (req, res) => {
  if (req.query.postId == null) {
    return res.status(400).json({ msg: "Parameter missing!!!", ack: 0 });
  }
  try {
    let filterData = {
      isDeleted: false,
      _id: mongoose.Types.ObjectId(req.query.postId),
    };
    const posts = await Post.findOneAndUpdate(filterData, { $set: { isDeleted: true } });
    if (posts) {
      res.status(200).json({
        data: posts,
        msg: "Post Details Updated successfully",
        ack: 1,
      });
    } else {
      res.status(200).json({ data: posts, msg: "No Post Found", ack: 0 });
    }
  } catch (err) {
    console.log("Error => ", err.message);
    res.status(500).json({ msg: "Something went wrong.." });
  }
};
export default { createPost, listsPosts, postDetails, postUpdate, postDelete };
