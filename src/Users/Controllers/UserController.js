import User from "../../Models/User";
var mongoose = require("mongoose");
/**
 * createUser
 * @param req body
 * @returns JSON
 */
const createUser = async (req, res) => {
  if (
    req.body.firstName == null ||
    req.body.lastName == null ||
    req.body.age == null ||
    req.body.gender == null
  ) {
    return res.status(400).json({ msg: "Parameter missing!!!", ack: 0 });
  }
  try {
    const allData = req.body;
    await new User(allData).save();
    res.status(200).json({ msg: "User Added Successfully", ack: 1 });
  } catch (err) {
    console.log("Error => ", err.message);
    res.status(500).json({ msg: "Something Went Wrong.", ack: 0 });
  }
};

/**
 * usersLists
 * @param req body
 * @returns json
 */
const usersLists = async (req, res) => {
  if (req.query.limit == null || req.query.page == null) {
    return res.status(400).json({ msg: "Parameter missing!!!", ack: 0 });
  }
  try {
    let filterData = {
      isActive: true,
      isDeleted: false,
    };
    let limit = parseInt(req.query.limit);
    let page = req.query.page;
    let skip = limit * page;
    const users = await User.aggregate([
      {
        $match: filterData,
      },
      { $sort: { createdDate: -1 } },
      { $skip: skip },
      { $limit: limit },
    ]);
    res
      .status(200)
      .json({ data: users, msg: "Users List found successfully", ack: 1 });
  } catch (err) {
    console.log("Error => ", err.message);
    res.status(500).json({ msg: "Something went wrong.." });
  }
};

/**
 * userDetails
 * @param req body
 * @returns json
 */
const userDetails = async (req, res) => {
  if (req.query.userId == null) {
    return res.status(400).json({ msg: "Parameter missing!!!", ack: 0 });
  }
  try {
    let filterData = {
      isActive: true,
      isDeleted: false,
      _id: mongoose.Types.ObjectId(req.query.userId),
    };
    const users = await User.findOne(filterData);
    if (users) {
      res
        .status(200)
        .json({ data: users, msg: "User Details found successfully", ack: 1 });
    } else {
      res.status(200).json({ data: users, msg: "No User Found", ack: 0 });
    }
  } catch (err) {
    console.log("Error => ", err.message);
    res.status(500).json({ msg: "Something went wrong.." });
  }
};

/**
 * userUpdate
 * @param req body
 * @returns json
 */
const userUpdate = async (req, res) => {
  if (req.query.userId == null) {
    return res.status(400).json({ msg: "Parameter missing!!!", ack: 0 });
  }
  try {
    const allData = req.body;
    let filterData = {
      isActive: true,
      isDeleted: false,
      _id: mongoose.Types.ObjectId(req.query.userId),
    };
    const users = await User.findOneAndUpdate(
      filterData,
      { $set: allData },
      {
        returnOriginal: false,
      }
    );
    if (users) {
      res
        .status(200)
        .json({
          data: users,
          msg: "User Details Updated successfully",
          ack: 1,
        });
    } else {
      res.status(200).json({ data: users, msg: "No User Found", ack: 0 });
    }
  } catch (err) {
    console.log("Error => ", err.message);
    res.status(500).json({ msg: "Something went wrong.." });
  }
};

/**
 * userUpdate
 * @param req body
 * @returns json
 */
const userDelete = async (req, res) => {
  if (req.query.userId == null) {
    return res.status(400).json({ msg: "Parameter missing!!!", ack: 0 });
  }
  try {
    let filterData = {
      isDeleted: false,
      _id: mongoose.Types.ObjectId(req.query.userId),
    };
    await User.findOneAndUpdate(filterData, { $set: { isDeleted: true } });
    res
      .status(200)
      .json({ msg: "User Deleted Successfully", ack: 1 });
  } catch (err) {
    console.log("Error => ", err.message);
    res.status(500).json({ msg: "Something went wrong.." });
  }
};

export default {
  createUser,
  usersLists,
  userDetails,
  userUpdate,
  userDelete,
};
